# Scripts LA

Ces scripts ont été écrit durant mon stage de 2 mois au Laboratoire d'Aérologie de Toulouse.

Ce stage se focalise sur la tempête Adrian ayant fortement frappé la Corse le 29 Octobre 2018.
Le but ici est de comparer les données observées in-situ avec les données du modèle numérique Méso-NH (LA/Météo-France).
