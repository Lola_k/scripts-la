import xarray as xr
import numpy as np
import matplotlib
matplotlib.use('Agg')
#matplotlib.use('pdf')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
import sys
import wrf

#EXP = str(sys.argv[1])
EXP = 'CNTRL'
N = '016'
VAR_name = 'THETAE'

### cross-section coordinates in geographical coordinates
lon_start = 7.
lat_start = 40.5
lon_end = 7.
lat_end = 43.5

###############################
### Read Meso-NH data
print("Read " + EXP + " Meso-NH simulation output")
###########################

ncfile = xr.open_dataset("/tmpdir/pantillo/"+EXP+"/"+EXP+".1.SEG01."+N+"DIA.nc", mask_and_scale=True, decode_times=True, decode_coords=True)

### read coordinates
time = ncfile.coords['time']
longitude = ncfile.coords['longitude'][0,:].values
latitude = ncfile.coords['latitude'][:,0].values

### define zoom
x0 = np.searchsorted( longitude, lon_start )
x1 = np.searchsorted( longitude, lon_end )
y0 = np.searchsorted( latitude, lat_start )
y1 = np.searchsorted( latitude, lat_end )

### compute wind speed
U = ncfile.data_vars['UT'][0,:,:,:]
V = ncfile.data_vars['VT'][0,:,:,:]
WIND = np.sqrt( U.values**2 + V.values**2 )

### read varible of interest
VAR = ncfile.data_vars[VAR_name][0,:,:,:]

### read sea level pressure
MSLP = ncfile.data_vars['MSLP']

### read orography
ZS =  ncfile.data_vars['ZS']

### read coordinates
time = ncfile.coords['time']
LON = ncfile.data_vars['LON']
LAT = ncfile.data_vars['LAT']
XHAT = ncfile.data_vars['XHAT']
YHAT = ncfile.data_vars['YHAT']
ALT = ncfile.data_vars['ALT']

###################################
### compute cross-section ###
print("compute cross-section")
#################################"

### define vertical levels
height_min = 0   # in m
height_max = 10000   # in m
height_interval = 100   # in m
height = np.linspace( height_min, height_max, height_interval )

### interpolate
VAR_section = wrf.vertcross( field3d=VAR.values, vert=ALT.values, start_point=wrf.CoordPair(x0,y0), end_point=wrf.CoordPair(x1,y1), levels=height )
WIND_section = wrf.vertcross( field3d=WIND, vert=ALT.values, start_point=wrf.CoordPair(x0,y0), end_point=wrf.CoordPair(x1,y1), levels=height )

### assign coordinates
#distance_max = np.sqrt( (XHAT[x1]-XHAT[x0])**2 + (YHAT[y1]-YHAT[y0])**2 ) * 1.e-3   # in km
#distance = np.linspace( 0, distance_max, VAR_section.sizes['dim_1'] )
if abs(lon_end-lon_start)>abs(lat_end-lat_start):
    LATLON_section = wrf.interpline( field2d=LON.values, start_point=wrf.CoordPair(x0,y0), end_point=wrf.CoordPair(x1,y1) )
else:
    LATLON_section = wrf.interpline( field2d=LAT.values, start_point=wrf.CoordPair(x0,y0), end_point=wrf.CoordPair(x1,y1) )

VAR_section.name = VAR.name
VAR_section['dim_0'] = height
#VAR_section['dim_1'] = distance
VAR_section['dim_1'] = LATLON_section.values

WIND_section.name = 'Wind speed (m/s)'
WIND_section['dim_0'] = height
#WIND_section['dim_1'] = distance
WIND_section['dim_1'] = LATLON_section.values

###################################
### plot figure ###
print("plot vertical cross-section")
#################################"

### open figure
plt.figure()
ax = plt.axes()

### plot surfaces
#surfaces = VAR_section.plot.contourf( cmap='YlOrRd', levels=10 )
surfaces = WIND_section.plot.contourf( cmap='YlOrRd', levels=10 )

### add contours
#contours = WIND_section.plot.contour( colors='blue', levels=range(10,100,10) )
contours = VAR_section.plot.contour( colors='blue' )
ax.clabel( contours, fmt='%1.0f')

#ax.set_xlabel("Distance (km)")
if abs(lon_end-lon_start)>abs(lat_end-lat_start): 
    ax.set_xlabel("Longitude (°E)")
else:
    ax.set_xlabel("Latitude")

ax.set_ylabel("Height (m)")
plt.title(time[0].values)

### save as png/pdf
plt.savefig( 'figure_' + EXP + '_N' + N + '_WIND_' + VAR_name + '_vertical_section.png' )

###################################
### plot figure ###
print("plot map")
#################################"

### open figure
plt.figure()
ax = plt.axes()

### assign coordinates
ZS.coords['ni'] = ZS.coords['longitude'][0,:].values
ZS.coords['nj'] = ZS.coords['latitude'][:,0].values

### plot surfaces
ZS.plot.contourf( cmap='terrain', levels=[0,10,20,50,100,200,500,1000,2000] )

### add geographical coordinates
ax.grid(True)
plt.xlabel('Longitude (°E)')
plt.ylabel('Latitude (°N)')

### add trace of section
plt.plot( [lon_start,lon_end], [lat_start,lat_end], linewidth=2, color='red' )

### save as png/pdf
plt.savefig( 'figure_' + EXP + '_N' + N + '_trace.png' )

