import xarray as xr
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import metpy
import metpy.calc as mpcalc
from metpy.plots import Hodograph, SkewT
from metpy.units import units
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
import sys
import gzip
import csv
import pandas as pd
import geopandas
import datetime
import matplotlib.dates as mdates

VAR_name = 'T2M'
var = 't'

station = 'Ajaccio-A�roport'
num_sta = 20004002

nj3 = 247
ni3 = 295

nj1 = 444
ni1 = 586

### Open files
# File M�teonet
f = open('/olympe2/p20024/kieffer/Donnees_MF/Meteonet/20181029.csv', 'rt', newline = '')
f = pd.read_csv(f)

KM3 = []
KM1 = []
varf = []
time = []
U_3 = []
V_3 = []
U_1 = []
V_1 = []
u_f = []
v_f = []

### FIGURE
plt.figure()
ax = plt.axes()

G = ["%.2d" % i for i in range(1,24)]

for i in G :
    
    H = str(int(i)+1).zfill(2)
    
    # Mod�les
    ncfile_3KM = xr.open_dataset("/tmpdir/pantillo/CNTRL/CNTRL.1.SEG01.0" + H + "DIA.nc", mask_and_scale=True, decode_times=True, decode_coords=True)
    
    ncfile_1KM = xr.open_dataset("/tmpdir/pantillo/HIRES/HIRES.2.HD1KM.0" + H + "DIA.nc", mask_and_scale=True, decode_times=True, decode_coords=True)
    
    ### VARIABLES
    VAR_3KM = ncfile_3KM.data_vars[VAR_name]
    VAR_1KM = ncfile_1KM.data_vars[VAR_name]
    
    time_3KM = ncfile_3KM.coords['time']
    time_1KM = ncfile_1KM.coords['time']
    
#    U_3KM = ncfile_3KM.data_vars['UM10']
#    V_3KM = ncfile_3KM.data_vars['VM10']
#    WIND_3KM = np.sqrt( U_3KM**2 + V_3KM**2 )
#    U_1KM = ncfile_1KM.data_vars['UM10']
#    V_1KM = ncfile_1KM.data_vars['VM10']
#    WIND_1KM = np.sqrt( U_1KM**2 + V_1KM**2 )
    
    for field in VAR_3KM, U_3KM, V_3KM, WIND_3KM:
        field.coords['ni'] = VAR_3KM.coords['longitude'][0,:].values
        field.coords['nj'] = VAR_3KM.coords['latitude'][:,0].values
    for field in VAR_1KM, U_1KM, V_1KM, WIND_1KM:
        field.coords['ni'] = VAR_1KM.coords['longitude'][0,:].values
        field.coords['nj'] = VAR_1KM.coords['latitude'][:,0].values
    
    ### DATES
    date_f = pd.to_datetime(f['date'])
    time_f = '20181029' + str(i) + '0000'
    d_sub = f[date_f == time_f]
    n_sta = d_sub[d_sub['number_sta']==num_sta]
    
    #uf = -n_sta['ff']*np.sin(n_sta['dd']*(np.pi/180))
    #vf = -n_sta['ff']*np.cos(n_sta['dd']*(np.pi/180))
    
    ### CONVERSIONS
    #var_f = n_sta[var]
    var_f = n_sta['t'] -273.15
    VAR_3KM = VAR_3KM -273.15
    VAR_1KM = VAR_1KM -273.15
    
    #KM3.append(WIND_3KM[0,nj3,ni3].values)
    #KM1.append(WIND_1KM[0,nj1,ni1].values)
    KM3.append(VAR_3KM[nj3, ni3].values)
    KM1.append(VAR_1KM[nj1, ni1].values)
    varf.append(var_f.values)
#    U_3.append(U_3KM[0,nj3,ni3].values)
#    V_3.append(V_3KM[0,nj3,ni3].values)
#    U_1.append(U_1KM[0,nj1,ni1].values)
#    V_1.append(V_1KM[0,nj1,ni1].values)
#    u_f.append(uf.values[0])
#    v_f.append(vf.values[0])
    time.append(time_3KM.values)
    


plt.plot(np.array(time), np.array(KM3), color = 'blue', linestyle = 'dashdot', label = 'MESO-NH 3KM')
plt.plot(np.array(time), np.array(KM1), color = 'yellow', linestyle = 'dashdot', label = 'MESO-NH 1KM')
plt.plot(np.array(time), np.array(varf), color = 'red', linestyle = 'dashdot', label = 'Station')
#ax.quiver(np.array(time), 1, np.array(U_3), np.array(V_3), color = 'blue', label = 'MESO-NH 3KM')
#ax.quiver(np.array(time), 1, np.array(U_1), np.array(V_1), color = 'yellow', label = 'MESO-NH 1KM')
#ax.quiver(np.array(time), 1, np.array(u_f), np.array(v_f), color = 'red', label = 'Station')

ax.xaxis.set_major_formatter(mdates.DateFormatter('%H'))
plt.xlabel('Temps (h)')
plt.ylabel(VAR_name + ' (' + VAR_3KM.units + ')')
plt.title('Suivi temporel 2018/10/29 ' + station, loc='left')
plt.title('Variable : ' + VAR_name, loc='right')
plt.legend(loc = 1)
#plt.close()
    
plt.savefig('Suivi_temp_' + station + '_' + VAR_name + '.png')
