import xarray as xr
import numpy as np
import matplotlib
matplotlib.use('Agg')
#matplotlib.use('pdf')
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
import sys

#EXP = str(sys.argv[1])
#EXP = 'ECM10'
N = '004'
VAR_name = 'FF10MAX'

###############################
### Read Meso-NH data
#print("Read " + EXP + " Meso-NH simulation output")
###########################

ncfile = xr.open_dataset("/tmpdir/pantillo/CNTRL/CNTRL.1.SEG01." + N + "DIA.nc", mask_and_scale=True, decode_times=True, decode_coords=True)

### compute wind speed
U = ncfile.data_vars['UT']
V = ncfile.data_vars['VT']
WIND = np.sqrt(U**2 + V**2)

### read varible of interest
VAR = ncfile.data_vars[VAR_name]

### read sea level pressure
MSLP = ncfile.data_vars['MSLP']

### read orography
ZS =  ncfile.data_vars['ZS']

### read coordinates
time = ncfile.coords['time']

### assign coordinates
for field in VAR, MSLP, ZS, U, V, WIND:
    field.coords['ni'] = VAR.coords['longitude'][0,:].values
    field.coords['nj'] = VAR.coords['latitude'][:,0].values

###################################
### plot figure ###
#print("plot variables")
#################################"

### open figure
plt.figure()
ax = plt.axes()

### plot surfaces
VAR[0,1:-1,1:-1].plot.contourf( cmap='YlOrRd', levels=10 )

### add contours
MSLP[0,1:-1,1:-1].plot.contour(colors='white',levels=range(950,1050,5))

### add wind vectors/barbs
interval_x = int(VAR['ni'].size / 10)
interval_y = int(VAR['nj'].size / 10)
ax.quiver(VAR['ni'][1:-1:interval_x], VAR['nj'][1:-1:interval_y], U[0,1:-1:interval_y,1:-1:interval_x], V[0,1:-1:interval_y,1:-1:interval_x], units = 'width', color='blue')

### add orography
ZS[1:-1,1:-1].plot.contour(colors='black',levels=[0])

### add geographical coordinates
ax.grid(True)
plt.xlabel('Longitude (°E)')
plt.ylabel('Latitude (°N)')

### add title
plt.title(time[0].values)

### save as png/pdf
plt.savefig( 'figure_N' + N + '_map.pdf' )

